import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Admin } from '../admin';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(
    public dialog: MatDialog,
    public http: HttpClient,
    public router: Router
  ) {}



  admin: Admin = new Admin();

  verification(): void {
    this.http.post('http://localhost:8084/admin', this.admin).subscribe(
      (data) => {
        console.log('success');
        // this.close();
        return this.router.navigateByUrl('/adminscreen');
      },
      (error) => {
        // this.close();
        alert('login failed');
      }
    );
  }
  close() {
    this.dialog.closeAll();
  }
}
