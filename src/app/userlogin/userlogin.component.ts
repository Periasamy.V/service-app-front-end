import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConnectionService } from '../connection.service';
import { Router } from '@angular/router';
import { CarService } from '../car-service';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css'],
})
export class UserloginComponent {
  constructor(
    public dialog: MatDialog,
    private router: Router,
    public http: ConnectionService
  ) {}
  close() {
    this.dialog.closeAll();
  }

  ngOnit() {}

  model: CarService = new CarService();
  getData: any;

  userLogin() {
    
    this.http.getUserData(this.model).subscribe(data=>{
      console.log(data);
      this.getData=data;
      return this.router.navigateByUrl('/userscreen');
    } ,  (error) => {
     
      alert('login failed');
    })
  }
 
}
