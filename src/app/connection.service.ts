import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CarService } from './car-service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ConnectionService {
  constructor(private http: HttpClient) {}

  postUser(carservice: CarService): Observable<CarService> {
    return this.http.post<CarService>('http://localhost:8084/user', carservice);
  }

  getUser(): Observable<CarService[]> {
    return this.http.get<CarService[]>('http://localhost:8084/user');
  }

  
  getUserData(user: CarService) {
    console.log(user);
    return this.http.post('http://localhost:8084/userlog', user);
  }

  postConfig(carservice: CarService): Observable<CarService> {
    return this.http.post<CarService>(
      'http://localhost:8084/adminconfig',
      carservice
    );
  }
  getConfig(): Observable<CarService[]> {
    return this.http.get<CarService[]>('http://localhost:8084/adminconfig');
  }
}
