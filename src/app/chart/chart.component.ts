import { Component } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
})
export class ChartComponent {
  public chart: any;

  ngOnInit() {
    this.chart = new Chart('MyChart', {
      type: 'pie',
      data :{
        labels: ['Red', 'Orange', 'Yellow', 'Green', 'Blue'],
        datasets: [
          {
            label: 'Data',
            data: [1, 4, 6, 8,5],
            backgroundColor: ['Red', 'Orange', 'Yellow', 'Green', 'Blue'],
          },
        ],
      },
    });
  }
}
