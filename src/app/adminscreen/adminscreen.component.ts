import { Component } from '@angular/core';
import { CarService } from '../car-service';
import { ConnectionService } from '../connection.service';
import { MatDialog } from '@angular/material/dialog';
import { AdminconfigComponent } from '../adminconfig/adminconfig.component';

@Component({
  selector: 'app-adminscreen',
  templateUrl: './adminscreen.component.html',
  styleUrls: ['./adminscreen.component.css'],
})
export class AdminscreenComponent {



  
  details: CarService[] = [];

  constructor(private http: ConnectionService,public dialog: MatDialog) {}
  ngOnInit() {
    this.http.getUser().subscribe((data: any) => {
      this.details = data;
    });
  }



  
  config() {
    this.dialog.open(AdminconfigComponent,{ width: '40%', height: '35%' });
  }
}
