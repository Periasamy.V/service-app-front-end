import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { StartingComponent } from './starting/starting.component';
import { LogComponent } from './log/log.component';
import { UserloginComponent } from './userlogin/userlogin.component';
import { AdminscreenComponent } from './adminscreen/adminscreen.component';
import { UserscreenComponent } from './userscreen/userscreen.component';
import { AdminconfigComponent } from './adminconfig/adminconfig.component';
import { ChartComponent } from './chart/chart.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'starting', component: StartingComponent },
  { path: 'log', component: LogComponent },
  { path: 'userlogin', component: UserloginComponent },
  { path: 'adminscreen', component: AdminscreenComponent },
  { path: 'userscreen', component: UserscreenComponent },
  { path: 'adminconfig', component: AdminconfigComponent },
  { path: 'chart', component: ChartComponent },
  { path: '', component: StartingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
