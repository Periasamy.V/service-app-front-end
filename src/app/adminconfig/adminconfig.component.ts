import { Component } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { CarService } from '../car-service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-adminconfig',
  templateUrl: './adminconfig.component.html',
  styleUrls: ['./adminconfig.component.css']
})
export class AdminconfigComponent {
  

  constructor(private http:ConnectionService ,private dialog:MatDialog){}

user:CarService=new CarService();
  saveMongo(){
    this.http.postConfig(this.user).subscribe((data)=>{
      console.log(this.user);
    })
  }

  close() {
    this.dialog.closeAll();
  }
}
