import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';
import { UserloginComponent } from '../userlogin/userlogin.component';


@Component({
  selector: 'app-starting',
  templateUrl: './starting.component.html',
  styleUrls: ['./starting.component.css'],
})
export class StartingComponent {
  constructor(public dialog: MatDialog) {}
 
  userlogin(): void {
    this.dialog.open(UserloginComponent, {
      width: '40%',
      height: '56.6%',
    });
  }

  adminlogin():void {
    this.dialog.open(LoginComponent, { width: '40%', height: '55.2%' });
  }
}
