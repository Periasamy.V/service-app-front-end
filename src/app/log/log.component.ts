import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConnectionService } from '../connection.service';
import { CarService } from '../car-service';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css'],
})
export class LogComponent {
  constructor(private http: ConnectionService, private router: Router ) {}
  usernameFormControl = new FormControl('', Validators.required);
  passwordFormControl = new FormControl('', Validators.required);
  emailFormControl = new FormControl('', [Validators.required,Validators.email]);
  selectFormControl = new FormControl('', Validators.required);

 

  user: CarService = new CarService();

  saveUser() {
    this.http.postUser(this.user).subscribe((data) => {
      this.router.navigateByUrl('/starting');
      console.log(this.user);
      console.log('registration');
    });
  }
 
}
