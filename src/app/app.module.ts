import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartingComponent } from './starting/starting.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { LoginComponent } from './login/login.component';
import { LogComponent } from './log/log.component';
import { UserloginComponent } from './userlogin/userlogin.component';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatPaginatorModule } from '@angular/material/paginator';


import { MatDialogModule } from '@angular/material/dialog';

import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserscreenComponent } from './userscreen/userscreen.component';
import { AdminscreenComponent } from './adminscreen/adminscreen.component';
import { HttpClientModule } from '@angular/common/http';

import { AdminconfigComponent } from './adminconfig/adminconfig.component';
import { ChartComponent } from './chart/chart.component';

@NgModule({
  declarations: [
    AppComponent,
    StartingComponent,

    LoginComponent,
    LogComponent,
    UserloginComponent,
    UserscreenComponent,
    AdminscreenComponent,
    AdminconfigComponent,
    ChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatMenuModule,
    MatDialogModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    HttpClientModule,
    FormsModule,
    MatPaginatorModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
