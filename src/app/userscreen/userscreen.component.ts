import { Component } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { CarService } from '../car-service';

@Component({
  selector: 'app-userscreen',
  templateUrl: './userscreen.component.html',
  styleUrls: ['./userscreen.component.css'],
})
export class UserscreenComponent {
  constructor(private http: ConnectionService) {}
  wprice: number = 50;
  sprice: number = 100;
  
  gst: number = 5;
  ans: number = 0;
  cars: any;
  car: CarService = new CarService();
 
  total: any;
  wtotal: any;
  
  wash() {
    this.http.getConfig().subscribe((data) => {
      this.cars = data;
      
      const r = this.cars[0].rating;
      const cr = this.cars[0].companyrating;
      this.ans = r * this.wprice * cr + this.gst;
      console.log(this.ans);
      
      this.wtotal=this.ans;
    });
  }
  sftotal: any;
  wftotal: any;
  wfprice: number = 100;
  fawash() {
    this.http.getConfig().subscribe((data) => {
      this.cars = data;

      const r = this.cars[0].rating;
      const cr = this.cars[0].companyrating;
      this.ans = r * this.wfprice * cr + this.gst;
      console.log(this.ans);
      
      this.wftotal=this.ans;
    });
  }
  sfutotal: any;
  wfutotal: any;
  wfuprice: number = 200;
  fuwash() {
    this.http.getConfig().subscribe((data) => {
      this.cars = data;

      const r = this.cars[0].rating;
      const cr = this.cars[0].companyrating;
      this.ans = r * this.wfuprice * cr + this.gst;
      console.log(this.ans);
      
      this.wfutotal=this.ans;
    });
  }
  
  
  
  
  
  
  service() {
    this.http.getConfig().subscribe((data) => {
      this.cars = data;
      
      const r = this.cars[0].rating;
      const cr = this.cars[0].companyrating;
      this.ans = r * this.sprice * cr + this.gst;
      console.log(this.ans);
      
      this.total=this.ans;
    });
  }
  fasprice: number = 150;
  faservice() {
    this.http.getConfig().subscribe((data) => {
      this.cars = data;

      const r = this.cars[0].rating;
      const cr = this.cars[0].companyrating;
      this.ans = r * this.fasprice * cr + this.gst;
      console.log(this.ans);

      this.sftotal=this.ans;
    });
  }
  fusprice: number = 250;
  fuservice() {
    this.http.getConfig().subscribe((data) => {
      this.cars = data;

      const r = this.cars[0].rating;
      const cr = this.cars[0].companyrating;
      this.ans = r * this.fusprice * cr + this.gst;
      console.log(this.ans);

      this.sfutotal=this.ans;
    });
  }
}
